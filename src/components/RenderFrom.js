import React from 'react'
import {useState} from 'react'
import { connect } from 'react-redux';
import { updateAction } from '../redux/action'


const RenderFrom = ({dispatchFormSubmitHandler}) => {
    const [firstName, setFistName] = useState('')
    const [lastName, setLastName] = useState('')
    const [phoneNo, setPhoneNo] = useState('')
  
    const formSubmitHandler = (e) =>{
      e.preventDefault()
      const formValues = {
        firstName,
        lastName,
        phoneNo
      }
      dispatchFormSubmitHandler(formValues)
      setFistName('')
      setLastName('')
      setPhoneNo('')
    }
    return (
        <>
             <h1>Personal Information </h1>
      <form onSubmit={formSubmitHandler}>
        <div className='mt'>
        <label>First Name:</label><br />
        <input value={firstName} 
               onChange={(e)=>setFistName(e.target.value)} 
               type="text"  />
        </div>
        <div className='mt'>
        <label>Last Name: </label><br />
        <input value={lastName} 
               onChange={(e)=>setLastName(e.target.value)} 
               type="text"  />
        </div>
        <div className='mt'>
          <label>Phone Number: </label><br />
        <input value={phoneNo} 
               onChange={(e)=>setPhoneNo(e.target.value)} 
               type="text"  />
        </div>
        <div className='mt'>
        <button type='submit'>Submit Form</button>
        </div>
      </form>
        </>
    )
}

const mapDispatchToProps = (dispatch) =>{
    return {
        dispatchFormSubmitHandler : (formValues) => dispatch(updateAction(formValues))
    }
  }

export default connect(null, mapDispatchToProps)(RenderFrom);

