import React from 'react'
import { connect } from 'react-redux';

const TodoList = ({todolist}) =>  
    todolist.map(todo => 
        <ul key={todo.phoneNo}>
            <li>{todo.firstName}</li>
            <li>{todo.lastName}</li>
            <li>{todo.phoneNo}</li>
        </ul>
    )


const RenderList = ({todolist = []}) =>
     (
        <>
        <h1>Todo List</h1>
        <TodoList todolist={todolist} />
        </>
    )
    
const mapStateToProps = (state) =>{
    return {
      todolist: state.todolist
    }
  }  
export default connect(mapStateToProps)(RenderList);
