import { UPDATE } from './actionType'

export const updateAction = (formValues) =>{
    return {type: UPDATE, payload: formValues}
}