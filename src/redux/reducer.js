import { UPDATE } from './actionType'

const initialState = {
    todolist:[]
}

export const reducer = (state = initialState, action) =>{
    switch (action.type) {
        case UPDATE:{
            return { 
                ...state,
                todolist: [...state.todolist, action.payload]
            }
        }
        default: return state
    }
}


