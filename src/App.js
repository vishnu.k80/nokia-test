import './App.css';
import RenderFrom from './components/RenderFrom'
import RenderList from './components/RenderList'

function App() {
  return (
    <div className="app">
      <RenderFrom />
      <RenderList />
    </div>
  );
}

export default App;
